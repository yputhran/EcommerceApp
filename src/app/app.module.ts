import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Input } from '@angular/core';
import {HttpModule} from "@angular/http";
import { ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ContainerModalComponent } from './container-modal/container-modal.component';
import { ShoppingCartComponent } from './container-modal/shopping-cart/shopping-cart.component';
import { PaymentComponent } from './container-modal/payment/payment.component';
import { ConfirmationComponent } from './container-modal/confirmation/confirmation.component';
import {ItemService} from './container-modal/shopping-cart/item.service';
import { ErrorPageComponent} from './error-page/error-page.component';
import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    CheckoutComponent,
    ContainerModalComponent,
    ShoppingCartComponent,
    PaymentComponent,
    ConfirmationComponent,
    ErrorPageComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [ItemService],
  bootstrap: [AppComponent]
})
export class AppModule {}
