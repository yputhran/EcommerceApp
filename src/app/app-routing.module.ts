import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { CheckoutComponent } from './checkout/checkout.component';
import { ContainerModalComponent } from './container-modal/container-modal.component';
import { ErrorPageComponent} from './error-page/error-page.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/checkout', pathMatch: 'full' },
  { path: 'checkout', component: CheckoutComponent },
  { path: 'shoppingModal',      component: ContainerModalComponent },
  {path: '404',  component: ErrorPageComponent}, 
 {path: '**', redirectTo: '/404'}
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes),
  ],
  declarations: [],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
