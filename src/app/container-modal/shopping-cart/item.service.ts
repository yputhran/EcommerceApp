import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Item} from "./Item";

@Injectable()
export class ItemService {
  private URL = "http://localhost:4200/assets/ItemList.json";
  constructor(private http:Http) { }

  getItems():Observable<Item[]> {
    return this.http
    .get(this.URL)
    .map((res:Response) => res.json().items)
    .catch((error:any) => Observable.throw(error.statusText));
  }
 
}
