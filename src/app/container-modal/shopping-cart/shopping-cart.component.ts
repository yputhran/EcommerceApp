import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Item } from './Item';
import { ItemService } from './item.service';
import * as _ from "lodash";
import { ArgumentType } from '@angular/core/src/view';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  items: Item[];
  invalidQtyFlag: boolean = false;
  @Input() TotalCost:number;
  @Output() outputToParent= new EventEmitter<number>()
  
  //selectedItems array stores the final list of items selected by the user.This could be required for a delivery component in the future.
  selectedItems = [];

  constructor(private itemService: ItemService) { }

  getItemList(): void {
    this.itemService.getItems()
      .subscribe(
      resultArray => this.items = resultArray,
      error => prompt("Error :: " + error)
      )
  }

  calculateTotalCost(): void {
    this.selectedItems.forEach(item => this.TotalCost += item.totalCost);
    this.outputToParent.emit(this.TotalCost);
  }

  quantityChange(event: any, currentItem): void {
    let quant: number = parseInt(event.target.value);
    if (quant > 0) {
      this.invalidQtyFlag = false;
      currentItem.qts = quant;
      currentItem.totalCost = currentItem.cost * quant;
      if (!this.selectedItems || !this.selectedItems.includes(currentItem)) {
        this.selectedItems.push(currentItem);
      }
      else {
        let index = this.selectedItems.indexOf(currentItem);
        this.selectedItems.splice(index, 1, currentItem);
      }
    }
    else {
      this.invalidQtyFlag = true;
    }
    this.calculateTotalCost();
  }

  ngOnInit() {
    this.getItemList();
  }

}
