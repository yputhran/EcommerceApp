import { Component, OnInit} from '@angular/core';
import {Item} from "./shopping-cart/Item";

@Component({
  selector: 'app-container-modal',
  templateUrl: './container-modal.component.html',
  styleUrls: ['./container-modal.component.css',
  './../app.component.css'
         ]
})
export class ContainerModalComponent implements OnInit {
  public visible = true;
  SCvisible = true;
  paymentVisible = false;
  confirmationVisible = false;
  TotalCost:number=0;
  constructor() { }
  showModal(): void {
    this.visible = true;
  }
  next(): void {
    this.SCvisible = !this.SCvisible;
    this.paymentVisible = !this.paymentVisible;
  }
  toConfirmation() : void{
    this.confirmationVisible = !this.confirmationVisible;
    this.paymentVisible = !this.paymentVisible;
  }
  getOutputVal(TotalCostChild:number){
    this.TotalCost=TotalCostChild;
  }
  ngOnInit() {
  }
}
