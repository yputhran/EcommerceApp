import { Component, OnInit, Input } from '@angular/core';
import { User } from './User';
import {FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  @Input() TotalCost:number;
  user = new FormGroup ({
    name:new FormControl(),
    email: new FormControl(),
    shippingAddress: new FormControl()
  });
  constructor() { }

  ngOnInit() {
  }

}
